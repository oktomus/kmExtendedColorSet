

#include <maya/MStatus.h>
#include <maya/MIntArray.h>
#include <maya/MPointArray.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshVertex.h>
#include <vector>
#include <chrono>

#include "kmUtil.hpp"
#include "kmColorSetTool.hpp"



const MStatus kmColorSetTool::addColorSet(MString colorSetName, const bool fill, MColor color){
  MStatus status = MS::kSuccess;

  status = defineNewColorSetName(colorSetName); // We generate a name to be sure that we modify the good color set
  MCheckStatus(status,"ERROR generatting new color set name");

  status = meshFn.createColorSetDataMesh(colorSetName);
  MCheckStatus(status,"ERROR creatting new color set");

  if(fill){
    status = fillColorSet(colorSetName, color);
    MCheckStatus(status,"ERROR filling color set");
  }

  return status;
}

const MStatus kmColorSetTool::blurColorSet(MString& colorSetName, const int size){
  MStatus status = MS::kSuccess;

  std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
  { // Test 012
    if(size>0){
      int nbVertex = meshFn.numVertices();

      std::vector<MIntArray> connectedVertices(nbVertex);
      MItMeshVertex verticesIt(mesh, &status);
      MCheckStatus(status,"ERROR cannot create vertices iterator");
      if(!status) return status;


      for (size_t i = 0; i < nbVertex; i++) {
        int dummy = 0;
        verticesIt.setIndex(i, dummy);
        status = verticesIt.getConnectedVertices(connectedVertices[verticesIt.index()]);
        MCheckStatus(status,"ERROR cannot get connected vertices");
      }


      MColor defaultColor(0,0,0,1);


      MColorArray colorsToWrite;
      meshFn.getVertexColors(colorsToWrite, &colorSetName, &defaultColor);

      #pragma omp parrallel for
      for (size_t i = 0; i < size; i++) {
        for (size_t k = 0; k < nbVertex; k++) {
          MIntArray *connectedVerticesId = &(connectedVertices[k]);
          MColor cTmp(0,0,0);
          // Get the colors
          cTmp.r=0;cTmp.g=0;cTmp.b=0;
          for (size_t l = 0; l < connectedVerticesId->length(); l++) {
            cTmp += colorsToWrite[(*connectedVerticesId)[l]];
          }
          // Do the average
          cTmp /= connectedVerticesId->length();
          // Stock the average
          colorsToWrite.set(cTmp, k);
        }
      }

      paintColorSet(colorSetName, colorsToWrite);

    }

  }

  std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
  cout << "Getting connected vertx with MItMeshVertex and apply blur in half_multi-thread took " << duration << "ms" << endl;








  return status;
}

const MStatus kmColorSetTool::closestVertices(const MPoint& point, const double& radius, MIntArray& resultArray, const bool& threaded) const{
  MStatus status = MS::kSuccess;

  status = resultArray.clear();
  MCheckStatus(status,"Error, cannot clear the result array");


  #pragma omp parallel for if(threaded)
  for (size_t i = 0; i < meshFn.numVertices(); i++) {
    MPoint verticeTmp;
    status = meshFn.getPoint(i, verticeTmp, MSpace::kWorld);
    MCheckStatus(status,"Error, cannot get vertice from mesh");

    if(verticeTmp.distanceTo(point)<radius){
      #pragma omp critical
        resultArray.append(i);
    }

  }

  return status;

}

const bool kmColorSetTool::colorSetExists(const MString& colorSetName) const{
  return meshFn.hasColorChannels(colorSetName);
}


const MStatus kmColorSetTool::defineNewColorSetName(MString& name){

  MStatus status = MS::kSuccess;

  if(name == ""){
    name = "kmColorSet";
  }

  unsigned int i = 1;
  MString basicName(name);

  while(meshFn.hasColorChannels(name, &status) && status){
    name = basicName + i;
    i++;
  }

  return status;
}


const MStatus kmColorSetTool::fillColorSet(const MString& colorSetName, MColor color){
  // This whole process is needed to fill a specific color set without using setCurrentColorSetName
  // setcurrentColorSetName doesn't work, it returns : No object exists
  // Otherwiser, a simple for loop from 0 to numVertices -1 does the job with setVertexColor(i, color)

  MStatus status = MS::kSuccess;

  MIntArray polygonVertexTmp; //

  for (size_t i = 0; i < meshFn.numPolygons(); i++) {
    status = meshFn.getPolygonVertices(i, polygonVertexTmp);  // Get the vertices of the polygon
    MCheckStatus(status,"Error cannot get polygon vertices");
    for(size_t j=0; j<polygonVertexTmp.length() && status; j++){
      status = meshFn.setColor(polygonVertexTmp[j], color, &colorSetName);  // Create a color for each
      MCheckStatus(status,"Error cannot set a color");
      status = meshFn.assignColor(i, j, polygonVertexTmp[j], &colorSetName);  // Assign the color
      //j represents the vertice id relatively to the polygon
      MCheckStatus(status,"Error cannot assign a color");
    }
  }
  return status;
}



const MStatus kmColorSetTool::paintColorSet(const MString& colorSetName, const MColorArray& colorsToWrite){
  MStatus status = MS::kSuccess;

  if (colorsToWrite.length() != meshFn.numVertices()){
    status.perror("The colors array is not the same length as the mesh vertices");
    return MS::kInvalidParameter;
  }


  #pragma omp parallel for
  for (size_t i = 0; i < meshFn.numPolygons(); i++) {
    MIntArray polygonVertexTmp; //
    status = meshFn.getPolygonVertices(i, polygonVertexTmp);  // Get the vertices of the polygon
    MCheckStatus(status,"Error cannot get polygon vertices");
    for(size_t j=0; j<polygonVertexTmp.length() && status; j++){
      int verticeIdTmp;
      verticeIdTmp = polygonVertexTmp[j];
      status = meshFn.setColor(polygonVertexTmp[j], colorsToWrite[verticeIdTmp], &colorSetName);  // Create a color for each
      MCheckStatus(status,"Error cannot set a color");
      status = meshFn.assignColor(i, j, verticeIdTmp, &colorSetName);  // Assign the color
      //j represents the vertice id relatively to the polygon
      MCheckStatus(status,"Error cannot assign a color");
    }
  }
  return status;
}
