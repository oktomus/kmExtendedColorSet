

#include <maya/MGlobal.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>

#include <maya/MDataHandle.h>

#include <maya/MFnAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>

#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnData.h>
#include <maya/MMeshIntersector.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MColorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MIntArray.h>
#include <maya/MDagPath.h>

#include <string>
#include <chrono>


#include "km_particlesToColorSet.hpp"

#include "kmParticleTool.hpp"
#include "kmColorSetTool.hpp"
#include "kmUtil.hpp"




// Input
MObject		km_particlesToColorSet::aInGeometry;
MObject		km_particlesToColorSet::aColorSetName;
MObject		km_particlesToColorSet::aParticlesPosition;
MObject		km_particlesToColorSet::aParticlesRadius;
MObject		km_particlesToColorSet::aParticlesColor;
MObject   km_particlesToColorSet::aParticleToCsCompound;
MObject 	km_particlesToColorSet::aMaxDistance;
MObject 	km_particlesToColorSet::aMaxInfluences;
MObject 	km_particlesToColorSet::aSearchApproximation;
MObject 	km_particlesToColorSet::aUseRadius;
MObject 	km_particlesToColorSet::aSlider;
// Output
MObject		km_particlesToColorSet::aOutGeometry;


void* km_particlesToColorSet::creator(){
	return new km_particlesToColorSet();
}

MStatus km_particlesToColorSet::initialize()
{
	MStatus				status = MS::kSuccess;
	MFnTypedAttribute tAttr;
	MFnNumericAttribute nAttr;
	MFnCompoundAttribute compoundAttr;
	MFnEnumAttribute eAttr;

	//inputs
	aParticlesPosition = tAttr.create("particlePosition", "pp", MFnData::kVectorArray, &status);
	MCheckStatus( status, "ERROR create aParticlesPosition" );
	status = addAttribute(aParticlesPosition);
	MCheckStatus( status, "ERROR adding aParticlesPosition" );

	aParticlesRadius = tAttr.create("particleRadius", "pr", MFnData::kDoubleArray, &status);
	MCheckStatus( status, "ERROR create aParticlesRadius" );
	status = addAttribute(aParticlesRadius);
	MCheckStatus( status, "ERROR adding aParticlesRadius" );



	aColorSetName = tAttr.create("colorSetName", "csn", MFnData::kString, &status);
	MCheckStatus( status, "ERROR create aColorSetName" );
	aParticlesColor = tAttr.create("particleColor", "pc", MFnData::kVectorArray, &status);
	MCheckStatus( status, "ERROR create aParticlesColor" );


	aParticleToCsCompound = compoundAttr.create("particleToColorSet", "ptc", &status);
	MCheckStatus( status, "ERROR creating color sets" );
	status = compoundAttr.addChild(aColorSetName);
	MCheckStatus( status, "ERROR adding child aColorSetName to aParticleToCsCompound" );
	status = compoundAttr.addChild(aParticlesColor);
	MCheckStatus( status, "ERROR adding child aParticlesColor to aParticleToCsCompound" );
	compoundAttr.setArray(true);
	compoundAttr.setDisconnectBehavior(MFnAttribute::kDelete);
	status = addAttribute(aParticleToCsCompound);
	MCheckStatus( status, "ERROR adding aParticleToCsCompound" );


	aInGeometry = tAttr.create("inGeometry", "ing", MFnMeshData::kMesh, &status);
	MCheckStatus( status, "ERROR creating ingeo" );
	status = addAttribute(aInGeometry);
	MCheckStatus( status, "ERROR adding ingeo" );

	// test
	aSlider = nAttr.create("update", "up", MFnNumericData::kFloat, 0, &status);
	MCheckStatus( status, "ERROR creating update" );
	status = addAttribute(aSlider);
	MCheckStatus( status, "ERROR adding upate" );

	aMaxDistance = nAttr.create("maxDistance", "max", MFnNumericData::kFloat, 5.0, &status);
	MCheckStatus( status, "ERROR creating max distance" );
	nAttr.setMin(0.0);
	nAttr.setSoftMax(10.0);
	status = addAttribute(aMaxDistance);
	MCheckStatus( status, "ERROR adding max distance" );

	aSearchApproximation = nAttr.create("searchApproximation", "sq", MFnNumericData::kInt, 0, &status);
	MCheckStatus( status, "ERROR creating searchApproximation" );
	nAttr.setMin(0);
	nAttr.setSoftMax(5);
	status = addAttribute(aSearchApproximation);
	MCheckStatus( status, "ERROR adding searchApproximation" );

	aMaxInfluences = nAttr.create("maxInfluences", "mi", MFnNumericData::kInt, -1, &status);
	MCheckStatus( status, "ERROR creating maxInfluences" );
	nAttr.setMin(-1);
	nAttr.setSoftMax(50);
	status = addAttribute(aMaxInfluences);
	MCheckStatus( status, "ERROR adding maxInfluences" );

	aUseRadius = nAttr.create("useRadius", "ura", MFnNumericData::kBoolean, false, &status);
	MCheckStatus( status, "ERROR creating use radius" );
	status = addAttribute(aUseRadius);
	MCheckStatus( status, "ERROR adding use radius" );

	//output
	aOutGeometry = tAttr.create("outGeometry", "oug", MFnMeshData::kMesh, &status);
	MCheckStatus( status, "ERROR creating outgeo" );
	tAttr.setWritable(false);
	tAttr.setStorable(false);
	status = addAttribute(aOutGeometry);
	MCheckStatus( status, "ERROR adding outGeo" );

	// dependencies
	status = attributeAffects(aColorSetName, aOutGeometry);
	MCheckStatus( status, "ERROR affect aColorSetName" );
	status = attributeAffects(aParticlesPosition, aOutGeometry);
	MCheckStatus( status, "ERROR affect aParticlesPosition" );
	status = attributeAffects(aParticlesRadius, aOutGeometry);
	MCheckStatus( status, "ERROR affect aParticlesRadius" );
	status = attributeAffects(aParticlesColor, aOutGeometry);
	MCheckStatus( status, "ERROR affect aParticlesColor" );
	status = attributeAffects(aInGeometry, aOutGeometry);
	MCheckStatus( status, "ERROR affect aInGeometry" );
	status = attributeAffects(aMaxDistance, aOutGeometry);
	MCheckStatus( status, "ERROR affect aMaxDistance" );
	status = attributeAffects(aSearchApproximation, aOutGeometry);
	MCheckStatus( status, "ERROR affect aSearchApproximation" );
	status = attributeAffects(aMaxInfluences, aOutGeometry);
	MCheckStatus( status, "ERROR affect aMaxInfluences" );
	status = attributeAffects(aUseRadius, aOutGeometry);
	MCheckStatus( status, "ERROR affect aUseRadius" );
	status = attributeAffects(aSlider, aOutGeometry);
	MCheckStatus( status, "ERROR affect aSlider" );

	return status;
}

km_particlesToColorSet::km_particlesToColorSet() {}
km_particlesToColorSet::~km_particlesToColorSet() {}

MStatus km_particlesToColorSet::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus status = MS::kSuccess;

	// Compute only if the output is requested
	if (plug == aOutGeometry){

		// Get the state of the node
		MDataHandle stateData = data.outputValue( state, &status );
		MCheckStatus( status, "ERROR getting state" );

		// Get the in mesh data
		MDataHandle inMeshData = data.inputValue(aInGeometry, &status);
		MCheckStatus(status,"ERROR getting aInGeometry");

		// Check if the mesh is real
		if (inMeshData.data().isNull()){
			displayError("Invalid attributes: inGeometry is required.");
			status = MS::kFailure;
			return status;
		}

		// Get the out mesh data
		MDataHandle outMeshHandle = data.outputValue(aOutGeometry, &status);
		MCheckStatus(status,"ERROR getting aOutGeometry");

		// Copy the in mesh to the output
		outMeshHandle.copy(inMeshData);

		// Create a function set for the out mesh
		MFnMesh meshFn(outMeshHandle.asMesh());


		// Get the compounds
		MArrayDataHandle inCompoundsData = data.inputArrayValue(aParticleToCsCompound, &status);
		MCheckStatus(status,"ERROR getting aParticleToCsCompound");
		unsigned int numCompound = inCompoundsData.elementCount();
		cout << numCompound << " to process " << endl;

		// If there is no color set to write, just stop here
		if (numCompound < 1) {
			return status;
		}

		// Get radius boolean
		const bool useRadius = data.inputValue(aUseRadius).asBool();

		// Get the max distance
		const float maxDistance = data.inputValue(aMaxDistance, &status).asFloat();
		MCheckStatus(status,"ERROR getting max distance");

		const int approximation = data.inputValue(aSearchApproximation, &status).asInt();
		const int maxInfluences = data.inputValue(aMaxInfluences, &status).asInt();

		// Get the particles positions
		MDataHandle positionHandle = data.inputValue(aParticlesPosition, &status);
		MCheckStatus(status,"ERROR getting aParticlesPosition");
		MObject positionData = positionHandle.data();
		if(positionData.isNull()){
			displayWarning("Invalid attributes: inParticlesPosition required");
			return status;
		}

		MFnVectorArrayData positionArrayData(positionData);
		MVectorArray particlesPoints = positionArrayData.array(&status);
		MCheckStatus(status,"ERROR getting particles positions array");

		// Get the particles radius
		MDataHandle radiusHandle = data.inputValue(aParticlesRadius, &status);
		MCheckStatus(status,"ERROR getting aParticlesRadius");
		MObject radiusData = radiusHandle.data();
		if(useRadius && radiusData.isNull()){
			displayWarning("Invalid attributes: inParticleRadius required");
			return status;
		}

		MFnDoubleArrayData radiusArrayData(radiusData);
		MDoubleArray particlesRadius;
		// Data needed from the mesh
		MObject meshObj(outMeshHandle.asMesh());
		// Color set tools
		kmColorSetTool meshFnEx(meshObj);
		// Particles tools
		kmParticleTool particleEx;


		// Do the real job
		size_t ppSize = particlesPoints.length();
		MIntArray particleForEachVertice;
		MVectorArray particlesColors;


		if(ppSize>0){
			// Init the particle tool
			status = particleEx.setPositions(particlesPoints);
			MCheckStatus(status,"ERROR setting particles positions in tool");
			if(!status){return status;}

			if(useRadius){
				particlesRadius = radiusArrayData.array(&status);
				MCheckStatus(status,"ERROR getting particles radius array");
				if(!status){return status;}

				status = particleEx.setRadius(particlesRadius);
				MCheckStatus(status,"ERROR setting particles radius in tool");
				if(!status){return status;}
			}

			status = particleEx.closestParticlesInRadius(maxDistance, meshObj, particleForEachVertice, useRadius, maxInfluences, approximation);
			MCheckStatus(status,"Error getting closestParticlesInRadius");
		}


		// Iterate over each wompound
		for (size_t i = 0; i < numCompound; i++) {
			try{

				// Get the current compound
				MDataHandle inCurrentCompound = inCompoundsData.inputValue();

				// Get the colorSet name of the compoud
				MString csName = inCurrentCompound.child(aColorSetName).asString();

				// If no name is given, set it to the node name
				if (csName == ""){
					//MGlobal::displayWarning("No colorSet name given for compound " + i);
					csName = MString(km_particlesToColorSet::name());
				}

				// Get the particles colors
				MDataHandle colorHandle = inCurrentCompound.child(aParticlesColor);
				MObject colorData = colorHandle.data();
				if(colorData.isNull()){
					displayWarning("Invalid attributes: inParticlesColor required, compound skipped.");
					throw StopOneProcess();
				}

				MFnVectorArrayData colorArrayData(colorData);
				particlesColors = colorArrayData.array(&status);
				MCheckStatus(status,"ERROR getting particles colors array");

				// Create the color set
				status = meshFnEx.addColorSet(csName);
				MCheckStatus(status,"Error creating new color set");

				if(ppSize>0){
					status = particleEx.setColors(particlesColors);
					MCheckStatus(status,"ERROR setting particles colors in tool");
					if(!status){return status;}

					MColorArray colorsToWrite;
					status = particleEx.relatedColors(particleForEachVertice, colorsToWrite);
					MCheckStatus(status,"Error getting particles related colors");

					status = meshFnEx.paintColorSet(csName, colorsToWrite);
					MCheckStatus(status,"Error painting color set");
				}


			}catch(const StopOneProcess e){
				continue;
			}

			// Swith to the next compound
			inCompoundsData.next();
		}

		data.setClean(plug);


	} else {
		return MStatus::kUnknownParameter;
	}

	return status;
}
