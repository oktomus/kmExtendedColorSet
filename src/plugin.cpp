#include <maya/MFnPlugin.h>

#include "km_createMultipleColorSet.hpp"
#include "km_particlesToColorSet.hpp"
#include "km_blurColorSet.hpp"

#define km_createMultipleColorSet_ID 0x00126bc0
#define km_particlesToColorSet_ID 0x00126bc1
#define km_blurColorSet_ID 0x00126bc2

MStatus initializePlugin(MObject obj)
{
	MStatus   status;

	MFnPlugin plugin(obj, "km_extendedColorSet - Kevin Masson", "1.0", "Any");

	status = plugin.registerNode("km_createMultipleColorSet", km_createMultipleColorSet_ID, km_createMultipleColorSet::creator, km_createMultipleColorSet::initialize);
	status = plugin.registerNode("km_particlesToColorSet", km_particlesToColorSet_ID, km_particlesToColorSet::creator, km_particlesToColorSet::initialize);
	status = plugin.registerNode("km_blurColorSet", km_blurColorSet_ID, km_blurColorSet::creator, km_blurColorSet::initialize);
	if (!status) { status.perror("registerNode"); return status; }



	return status;
}

MStatus uninitializePlugin(MObject obj)
{
	MStatus   status;
	MFnPlugin plugin(obj);

	status = plugin.deregisterNode(km_blurColorSet_ID);
	status = plugin.deregisterNode(km_particlesToColorSet_ID);
	status = plugin.deregisterNode(km_createMultipleColorSet_ID);
	if (!status) { status.perror("deregisterNode"); return status; }


	return status;
}
