#ifndef NODE_PARTICLES_TO_COLOR_SET
#define NODE_PARTICLES_TO_COLOR_SET

#include <maya/MPxNode.h>

class MPlug;
class MDataBlock;


//creation de la classe du node exempleNode
class km_particlesToColorSet : public MPxNode
{
  public:
    km_particlesToColorSet();
    virtual				~km_particlesToColorSet();
    virtual MStatus		compute(const MPlug& plug, MDataBlock& data);
    static  void*		creator();
    static  MStatus		initialize();

  private:
    //inputs
    static  MObject		aInGeometry;  // The input mesh
    static  MObject		aColorSetName;  // The color set name
    static  MObject		aParticlesPosition;  // The particles position
    static  MObject		aParticlesRadius;  // The particles radius
    static  MObject		aParticlesColor;  // The particles colors (can be velocity or anything else)
    static  MObject   aParticleToCsCompound;  // The differents color sets to write
    static  MObject   aMaxDistance;  // The max distance of a particle from a vertice
    static  MObject   aSearchApproximation;
    static  MObject   aMaxInfluences;  
    static  MObject   aUseRadius;  // Use the particles radius or not
    //outputs
    static  MObject		aOutGeometry;  // The output mesh

    // test
    static MObject aSlider;  // To update the node

};

#endif
