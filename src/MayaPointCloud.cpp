#include "MayaPointCloud.hpp"

#include <maya/MPointArray.h>
#include <maya/MPoint.h>
#include <maya/MFnMesh.h>

#include "kmUtil.hpp"

void generateMPointCloud(MayaPointCloud &pc, MPointArray& source)
{
	pc.points.resize(source.length());
  #pragma omp parallel for
  for (size_t i = 0; i < source.length(); i++) {
    pc.points[i].x = source[i].x;
    pc.points[i].y = source[i].y;
    pc.points[i].z = source[i].z;
  }
}

void generateMPointCloudFromVertices(MayaPointCloud &pc, MFnMesh& meshFn)
{
  MStatus status = MStatus::kSuccess;
  MPointArray vertices;
	status = meshFn.getPoints(vertices,MSpace::kWorld);
  MCheckStatus(status,"Error cannot assing mesh vertices to point cloud");
  generateMPointCloud(pc, vertices);
}
