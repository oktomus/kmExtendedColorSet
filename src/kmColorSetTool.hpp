#ifndef KM_COLORSETTOOL
#define KM_COLORSETTOOL

class MFnMesh;
class MString;
class MColor;
class MColorArray;
class MIntArray;
class MPoint;

/** Tools to manage color sets on a mesh
*
*/
class kmColorSetTool{
public:

  /** The tool constructor
  * \param mesh   a refrence to the mesh to work with
  */
  kmColorSetTool(MObject& mesh_) : mesh(mesh_), meshFn(mesh_){};

  /** Add a color set to the mesh
  * If the name is not available, modify the given parameter
  * \param colorSetName   MString   the color set name
  * \param fill           bool      fill the color set with color if true
  * \param color          MColor    The color to use to fill the color set if fill is true
  */
  const MStatus addColorSet(MString colorSetName, const bool fill = true, MColor color = MColor(0,0,0,1));

  /** Blur a given color set if exists
  * For each vertice, apply the average color of all vertices connected to it
  * \param colorSetName   MString   the color set to blur if it exists
  * \param size           int       The size of the blur
  */
  const MStatus blurColorSet(MString& colorSetName, const int size=1);

  /** Create an array with all vertices id avaible at a given radius of a point
  * \param point    MPoint, the center from where we search the vertices
  * \param radius   MPoint, The max distance from the point in which vertices can be
  * \param resultArray   MPointArray, The resultArray
  */
  const MStatus closestVertices(const MPoint& point, const double& radius, MIntArray& resultArray, const bool& threaded=false) const;

  /** Check if a given color set exists
  * \param colorSetName MString the color set name to check
  */
  const bool colorSetExists(const MString& colorSetName) const;

  /** Define a new color set name
  * Check if a color set already exists and generate a name until this one is available
  * \param colorSetName   MString   Base name
  */
  const MStatus defineNewColorSetName(MString& name);

  /** Fill a specific color set
  * \param color          MColor    Color to fill
  * \param colorSetName   MColor    Colorset to fill
  */
  const MStatus fillColorSet(const MString& colorSetName, MColor color = MColor(0,0,0,1));

  /** Paint a specific color set
  * \param colorSetName   MColor        Colorset to fill
  * \param colorsToWrite  MColorArray   An array of the size of the mesh vertices count -1, with for each vertice, the color to write
  */
  const MStatus paintColorSet(const MString& colorSetName, const MColorArray& colorsToWrite);

private:

  /** The mesh function set to use in the toolset
  */
  MFnMesh meshFn;
  /** The mesh the tool is working on
  */
  MObject& mesh;

};


#endif // KM_COLORSETTOOL
