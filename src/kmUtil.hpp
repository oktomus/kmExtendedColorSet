#ifndef KM_UTIL
#define KM_UTIL

#include <sstream>


class MStatus;




// Exceptions
struct StopOneProcess : std::exception {
  char const* what() const throw();
};

MStatus MCheckStatus(const MStatus status, const std::string message);

void displayError(const std::string message);

void displayWarning(const std::string message);

void debug(const std::string message);

template <typename T>
std::string intToString(const T number);


// std

int firstElement( const std::pair<int, int> &p );

#endif
