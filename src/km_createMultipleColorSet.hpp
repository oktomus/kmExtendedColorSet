#ifndef __NODE_CREATE_MULTIPLE_COLOR_SET
#define __NODE_CREATE_MULTIPLE_COLOR_SET

#include <maya/MPxNode.h>

class MPlug;
class MDataBlock;

//creation de la classe du node exempleNode
class km_createMultipleColorSet : public MPxNode
{
  public:
    km_createMultipleColorSet();
    virtual				~km_createMultipleColorSet();
    virtual MStatus		compute(const MPlug& plug, MDataBlock& data);
    static  void*		creator();
    static  MStatus		initialize();

  private:
    //inputs
    static  MObject		aInGeometry;  // The input mesh
    static  MObject		aColorSetName;  // The color set name
    static  MObject		aColorSetColor;  // The color set color
    static  MObject		aCompoundColorSets;  // The color sets array
    //outputs
    static  MObject		aOutGeometry;  // The output meshes

    // test
    static MObject aSlider;

};

#endif
