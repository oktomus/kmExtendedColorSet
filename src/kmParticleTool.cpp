
#include <maya/MFnMesh.h>
#include <maya/MStatus.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MIntArray.h>
#include <list>

#include "kmUtil.hpp"
#include "nanoflann.hpp"
#include "MayaPointCloud.hpp"
#include "kmParticleTool.hpp"
#include "kmColorSetTool.hpp"

const MStatus kmParticleTool::closestParticlesInRadius(const double& maxDistance, MObject& mesh, MIntArray& resultArray, const bool useRadius, const int& maxInfluence, const int& approximation) const{
  MStatus status = MStatus::kSuccess;

  MFnMesh meshFn(mesh);
  size_t nbVertex = meshFn.numVertices();

  resultArray = MIntArray(nbVertex, -1); // Create an array filled with -1, which means, no particle for each vertice

  if(maxInfluence==0){
    return MS::kSuccess;
  }

  if(useRadius && !isValidRadius()){
    displayWarning("A array of radius is needed");
    return MS::kFailure;
  }

  kmColorSetTool meshFnEx(mesh);

  MayaPointCloud cloud;

  const bool radiusSearch = (maxInfluence<0);

  generateMPointCloudFromVertices(cloud, meshFn);

  typedef nanoflann::KDTreeSingleIndexAdaptor<
  nanoflann::L2_Simple_Adaptor<double, MayaPointCloud>,
  MayaPointCloud,
  3
  > kd_tree;

  kd_tree index(3, cloud, nanoflann::KDTreeSingleIndexAdaptorParams(10));
  index.buildIndex();


  nanoflann::SearchParams params(32,approximation,false);
  if(!radiusSearch){
    params.sorted = true;
  }



  size_t ppSize = positions->length();

  std::vector<std::list<int> > temp(ppSize);


  // For each particle
  #pragma omp parallel for
  for (size_t i = 0; i < ppSize; i++) {
    double distance;
    if(useRadius){
      distance = (*radius)[i];
    }else{
      distance = maxDistance;
    }
    MVector particle = (*positions)[i];
    double query_pt[3] = {particle.x, particle.y, particle.z};
    // Get the closest vertices ids in a radius
    if(radiusSearch){
      std::vector<std::pair<size_t, double> > ret_matches;
      if(useRadius){
        const size_t nMatches = index.radiusSearch(&query_pt[0], distance, ret_matches, params);
      }else{
        const size_t nMatches = index.radiusSearch(&query_pt[0], distance, ret_matches, params);
      }
      // Add vertices ids to the particle block
      for(auto it=ret_matches.begin(); it != ret_matches.end();it++){
        temp[i].insert(temp[i].end(), it->first);
      }
    }else{
      std::vector<size_t>   ret_index(maxInfluence);
      std::vector<double> out_dist_sqr(maxInfluence);
      index.knnSearch(&query_pt[0], maxInfluence, &ret_index[0], &out_dist_sqr[0]);
      for (size_t q=0;q<maxInfluence;q++){
        if(out_dist_sqr[q]<maxDistance){
          temp[i].insert(temp[i].end(), ret_index[q]);
        }else{
          break;
        }
      }
      cout << "\n";

    }
  }

  std::list<int> idsTemp;
  int currentVertIdTmp, currentPIdTmp;
  bool addParticle;
  MPoint currentVertTmp, currentPTmp;
  MVector particle;
  for (size_t i = 0; i < temp.size(); i++) {
    particle = (*positions)[i];
    idsTemp = temp[i];
    for (auto it = idsTemp.begin(); it != idsTemp.end(); it++) {
      currentVertIdTmp = *it;
      // Get the particle id already at this vertex
      currentPIdTmp = resultArray[currentVertIdTmp];
      // Checking if this particle is the first
      addParticle = !isValidId(&currentPIdTmp);
      // If not Checking if this particle is closer to the vertice
      if(!addParticle){
        status = meshFn.getPoint(currentVertIdTmp, currentVertTmp, MSpace::kWorld);
        MCheckStatus(status,"Error cannot get vertice point");
        currentPTmp = (*positions)[currentPIdTmp];
        addParticle =  (currentVertTmp.distanceTo(particle) < currentVertTmp.distanceTo(currentPTmp));
      }
      // If, replace the particle id by the current one
      if(addParticle){
        resultArray[currentVertIdTmp] = i;
      }
    }
  }



  return status;
}



const bool kmParticleTool::isValidId(const int *id) const{
  if(positions == NULL) return false;
  if(*id <0) return false;
  if(*id >= positions->length()) return false;
  return true;
}

const bool kmParticleTool::isValidPosition() const{
  if(positions == NULL) return false;
  if(positions->length() < 0) return false;
  return true;
}

const bool kmParticleTool::isValidColor() const{
  if(!isValidPosition()) return false;
  if(colors == NULL) return false;
  if(colors->length() != positions->length()) return false;
  return true;
}

const bool kmParticleTool::isValidRadius() const{
  if(!isValidPosition()) return false;
  if(radius == NULL) return false;
  if(radius->length() != positions->length()) return false;
  return true;
}

const MStatus kmParticleTool::relatedColors(MIntArray& particlesId, MColorArray& resultArray, MColor defaultColor) const{
  MStatus status = MStatus::kSuccess;
  resultArray.setLength(particlesId.length());

  #pragma omp parallel for
  for (size_t i = 0; i < particlesId.length(); i++) {
    int pId;
    MColor cTmp;
    MVector vTmp;
    pId = particlesId[i];
    if(isValidId(&pId)){
      vTmp = (*colors)[pId];
      cTmp = MColor(vTmp.x, vTmp.y, vTmp.z);
    }else{
      cTmp = defaultColor;
    }
    resultArray[i] = cTmp;
  }
  return status;
}

const MStatus kmParticleTool::setColors(MVectorArray& colors_){
  if(!isValidPosition()){
    displayWarning("You must set the particles position before anything else");
    return MStatus::kFailure;
  }

  if(colors_.length() != positions->length()){
    displayWarning("The particles color array must be of the same size as the positions array");
    return MStatus::kFailure;
  }

  colors = &colors_;
  return MStatus::kSuccess;
}

const MStatus kmParticleTool::setPositions(MVectorArray& positions_){
  positions = &positions_;
  return MStatus::kSuccess;
}

const MStatus kmParticleTool::setRadius(MDoubleArray& radius_){
  if(!isValidPosition()){
    displayWarning("You must set the particles position before anything else");
    return MStatus::kFailure;
  }

  if(radius_.length() != positions->length()){
    displayWarning("The particles radius array must be of the same size as the positions array");
    return MStatus::kFailure;
  }

  radius = &radius_;
  return MStatus::kSuccess;
}
