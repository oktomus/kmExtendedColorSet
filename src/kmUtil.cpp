#include "kmUtil.hpp"

#include <maya/MGlobal.h>
#include <maya/MStatus.h>
#include <maya/MString.h>

// Exceptions
char const* StopOneProcess::what() const throw(){
  return "A part of the process failed but the whole process continue.";
};

MStatus MCheckStatus(const MStatus status, const std::string message) {
if( MStatus::kSuccess != status ) {
	const MString msMessage(message.c_str());
	const MString msError = status.errorString();
	MGlobal::displayError(msMessage + ": " + msError);
}
return status;
}

void displayError(const std::string message){
	MGlobal::displayError(message.c_str());
}

void displayWarning(const std::string message){
	MGlobal::displayWarning(message.c_str());
}

void debug(const std::string message){
  cout << message << endl;
}

template <typename T>
std::string intToString(const T number){
	std::stringstream ss;
	ss << number;
	return ss.str();
}

// std

int firstElement( const std::pair<int, int> &p ) {
    return p.first;
}
