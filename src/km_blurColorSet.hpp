#ifndef __NODE_BLURE_COLOR_SET
#define __NODE_BLURE_COLOR_SET

#include <maya/MPxNode.h>

class MPlug;
class MDataBlock;

class km_blurColorSet : public MPxNode
{
  public:
    km_blurColorSet();
    virtual				~km_blurColorSet();
    virtual MStatus		compute(const MPlug& plug, MDataBlock& data);
    static  void*		creator();
    static  MStatus		initialize();

  private:
    //inputs
    static  MObject		aInGeometry;  // The input mesh
    static  MObject		aColorSetsNames;  // The color sets names to blur
    static  MObject   aBlur;  // The quantity of blur
    //outputs
    static  MObject		aOutGeometry;  // The output mesh

    // test
    static MObject aSlider;  // To update the node

};

#endif
