

#include <maya/MGlobal.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>

#include <maya/MDataHandle.h>

#include <maya/MFnAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>

#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnData.h>
#include <maya/MMeshIntersector.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshVertex.h>
#include <maya/MFnMeshData.h>
#include <maya/MColorArray.h>
#include <maya/MIntArray.h>
#include <maya/MDagPath.h>

#include <string>

#include "kmUtil.hpp"
#include "km_blurColorSet.hpp"

#include "kmColorSetTool.hpp"





// Input
MObject		km_blurColorSet::aInGeometry;
MObject 	km_blurColorSet::aColorSetsNames;
MObject   km_blurColorSet::aBlur;
MObject 	km_blurColorSet::aSlider;
// Output
MObject		km_blurColorSet::aOutGeometry;


void* km_blurColorSet::creator(){
	return new km_blurColorSet();
}

MStatus km_blurColorSet::initialize()
{
	MStatus				status = MS::kSuccess;
	MFnTypedAttribute tAttr;
	MFnNumericAttribute nAttr;
	MFnCompoundAttribute compoundAttr;
	MFnEnumAttribute eAttr;

	//inputs

	aBlur = nAttr.create("blur", "blr", MFnNumericData::kInt, 1, &status);
	MCheckStatus( status, "ERROR creating blur" );
	nAttr.setMin(0);
	nAttr.setSoftMin(0);
	nAttr.setSoftMax(10);
	status = addAttribute(aBlur);
	MCheckStatus( status, "ERROR adding blur" );


	aColorSetsNames = tAttr.create("colorSetsNames", "csn", MFnData::kString, &status);
	MCheckStatus( status, "ERROR create aColorSetsNames" );
	tAttr.setArray(true);
	status = addAttribute(aColorSetsNames);
	MCheckStatus( status, "ERROR adding aColorSetsNames" );


	aInGeometry = tAttr.create("inGeometry", "ing", MFnMeshData::kMesh, &status);
	MCheckStatus( status, "ERROR creating ingeo" );
	status = addAttribute(aInGeometry);
	MCheckStatus( status, "ERROR adding ingeo" );

	// test
	aSlider = nAttr.create("update", "up", MFnNumericData::kFloat, 0, &status);
	MCheckStatus( status, "ERROR creating update" );
	status = addAttribute(aSlider);
	MCheckStatus( status, "ERROR adding upate" );

	//output
	aOutGeometry = tAttr.create("outGeometry", "oug", MFnMeshData::kMesh, &status);
	MCheckStatus( status, "ERROR creating outgeo" );
	tAttr.setWritable(false);
	tAttr.setStorable(false);
	status = addAttribute(aOutGeometry);
	MCheckStatus( status, "ERROR adding outGeo" );

	// dependencies
	status = attributeAffects(aColorSetsNames, aOutGeometry);
	MCheckStatus( status, "ERROR affect aColorSetsNames" );
	status = attributeAffects(aBlur, aOutGeometry);
	MCheckStatus( status, "ERROR affect aBlur" );
	status = attributeAffects(aInGeometry, aOutGeometry);
	MCheckStatus( status, "ERROR affect aInGeometry" );
	status = attributeAffects(aSlider, aOutGeometry);
	MCheckStatus( status, "ERROR affect aSlider" );

	return status;
}

km_blurColorSet::km_blurColorSet() {}
km_blurColorSet::~km_blurColorSet() {}

MStatus km_blurColorSet::compute( const MPlug& plug, MDataBlock& data )
{
	cout << "Computing" << endl;
	MStatus status = MS::kSuccess;

	// Compute only if the output is requested
	if (plug == aOutGeometry){

		// Get the state of the node
		MDataHandle stateData = data.outputValue( state, &status );
		MCheckStatus( status, "ERROR getting state" );

		// Get the in mesh data
		MDataHandle inMeshData = data.inputValue(aInGeometry, &status);
		MCheckStatus(status,"ERROR getting aInGeometry");

		// Check if the mesh is real
		if (inMeshData.data().isNull()){
			displayError("Invalid attributes: inGeometry is required.");
			status = MS::kFailure;
			return status;
		}

		// Get the out mesh data
		MDataHandle outMeshHandle = data.outputValue(aOutGeometry, &status);
		MCheckStatus(status,"ERROR getting aOutGeometry");

		// Copy the in mesh to the output
		outMeshHandle.copy(inMeshData);

		// Create a function set for the out mesh
		MFnMesh meshFn(outMeshHandle.asMesh());


		// Get the color sets to blur
		MArrayDataHandle csData = data.inputArrayValue(aColorSetsNames, &status);
		MCheckStatus(status,"ERROR getting aColorSetsNames");

		int numCs = csData.elementCount();

		// If there is no color set to blur, just stop here
		if (numCs < 1) {
			return status;
		}

		// Get the blur size
		int blur = data.inputValue(aBlur, &status).asInt();
		MCheckStatus(status,"ERROR getting blur size");

		if (blur <1){
			return status;
		}

		// Data needed from the mesh
		int nbVertex = meshFn.numVertices();

		MObject meshObj(outMeshHandle.asMesh());

		// Color set tools
		kmColorSetTool meshFnEx(meshObj);



		// Iterate over each colorSet
		for (size_t i = 0; i < numCs; i++) {

			// Debug
			cout << "Working on compound " << i << endl;
			try{

				MDataHandle currentCs = csData.inputValue();
				// Get the colorSet name of the compoud
				MString csName = currentCs.asString();

				// If no name is given, set it to the node name
				if (!meshFnEx.colorSetExists(csName)){
					MGlobal::displayWarning("Color set does not exists, compound " + i);
					return MStatus::kInvalidParameter;
				}

				cout << "Bluring color set " << csName << endl;
				meshFnEx.blurColorSet(csName, blur);


			}catch(const StopOneProcess e){
				continue;
			}

			// Swith to the next color set
			if(!csData.next()){
				break;
			}
		}

		data.setClean(plug);


	} else {
		return MStatus::kUnknownParameter;
	}

	return status;
}
