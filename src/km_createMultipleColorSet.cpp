

#include <maya/MGlobal.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>

#include <maya/MDataHandle.h>

#include <maya/MFnAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MColorArray.h>
#include <maya/MFloatVector.h>
#include <maya/MIntArray.h>


#include "km_createMultipleColorSet.hpp"

#include "kmUtil.hpp"
#include "kmColorSetTool.hpp"





//inputs
MObject     km_createMultipleColorSet::aInGeometry;
MObject     km_createMultipleColorSet::aColorSetName;
MObject     km_createMultipleColorSet::aColorSetColor;
MObject     km_createMultipleColorSet::aCompoundColorSets;
//output
MObject     km_createMultipleColorSet::aOutGeometry;
// test
MObject     km_createMultipleColorSet::aSlider;


void* km_createMultipleColorSet::creator(){
	return new km_createMultipleColorSet();
}

MStatus km_createMultipleColorSet::initialize()
{
	MStatus				status = MS::kSuccess;
	MFnTypedAttribute tAttr;
	MFnNumericAttribute nAttr;
	MFnCompoundAttribute compoundAttr;

	//inputs

	aColorSetName = tAttr.create("colorSetName", "csn", MFnData::kString, &status);
	MCheckStatus( status, "ERROR create aColorSetName" );

	aColorSetColor = nAttr.createColor("colorSetColor", "csc", &status);
	MCheckStatus( status, "ERROR create aColorSetColor" );

	aCompoundColorSets = compoundAttr.create("colorSets", "css", &status);
	MCheckStatus( status, "ERROR creating color sets" );
	status = compoundAttr.addChild(aColorSetName);
	MCheckStatus( status, "ERROR adding child aColorSetName to aCompoundColorSets" );
	status = compoundAttr.addChild(aColorSetColor);
	MCheckStatus( status, "ERROR adding child aColorSetColor to aCompoundColorSets" );
	compoundAttr.setArray(true);
	compoundAttr.setDisconnectBehavior(MFnAttribute::kDelete);
	status = addAttribute(aCompoundColorSets);
	MCheckStatus( status, "ERROR adding aCompoundColorSets" );


	aInGeometry = tAttr.create("inGeometry", "ing", MFnMeshData::kMesh, &status);
	MCheckStatus( status, "ERROR creating ingeo" );
	status = addAttribute(aInGeometry);
	MCheckStatus( status, "ERROR adding ingeo" );

	// test
	aSlider = nAttr.create("update", "up", MFnNumericData::kFloat, 0, &status);
	MCheckStatus( status, "ERROR creating update" );
	status = addAttribute(aSlider);
	MCheckStatus( status, "ERROR adding upate" );

	//output
	aOutGeometry = tAttr.create("outGeometry", "oug", MFnMeshData::kMesh, &status);
	MCheckStatus( status, "ERROR creating outgeo" );
	tAttr.setWritable(false);
	tAttr.setStorable(false);
	status = addAttribute(aOutGeometry);
	MCheckStatus( status, "ERROR adding outGeo" );

	// dependencies
	status = attributeAffects(aColorSetName, aOutGeometry);
	MCheckStatus( status, "ERROR affect aColorSetName" );
	status = attributeAffects(aColorSetColor, aOutGeometry);
	MCheckStatus( status, "ERROR affect aColorSetColor" );
	status = attributeAffects(aInGeometry, aOutGeometry);
	MCheckStatus( status, "ERROR affect aInGeometry" );
	status = attributeAffects(aSlider, aOutGeometry);
	MCheckStatus( status, "ERROR affect aSlider" );

	return status;
}

km_createMultipleColorSet::km_createMultipleColorSet() {}
km_createMultipleColorSet::~km_createMultipleColorSet() {}

MStatus km_createMultipleColorSet::compute( const MPlug& plug, MDataBlock& data )
{
	cout << "Computing" << endl;
	MStatus status = MS::kSuccess;

	if (plug == aOutGeometry){
		MDataHandle stateData = data.outputValue( state, &status );
		MCheckStatus( status, "ERROR getting state" );

		MDataHandle inMeshData = data.inputValue(aInGeometry, &status);
		MCheckStatus(status,"ERROR getting aInGeometry");

		if (inMeshData.data().isNull()){
			displayError("Invalid attributes: inGeometry is required.");
			status = MS::kFailure;
			return status;
		}

		MDataHandle outMeshHandle = data.outputValue(aOutGeometry, &status);
		MCheckStatus(status,"ERROR getting aOutGeometry");


		outMeshHandle.copy(inMeshData);

		MFnMesh meshFn(outMeshHandle.asMesh());

		MObject meshObj(outMeshHandle.asMesh());

		// Color set tools
		kmColorSetTool meshFnEx(meshObj);


		MArrayDataHandle inCsHandle = data.inputArrayValue(aCompoundColorSets, &status);
		MCheckStatus(status,"ERROR getting aCompoundColorSets");
		unsigned int numCs = inCsHandle.elementCount();

		for (size_t i = 0; i < numCs; i++) {
			MDataHandle inCsData = inCsHandle.inputValue();
			MString csName = inCsData.child(aColorSetName).asString();
			MFloatVector csColorVector = inCsData.child(aColorSetColor).asFloatVector();

			MColor csColor(csColorVector.x, csColorVector.y, csColorVector.z);

			// If no name is given, set it to the node name
			if (csName == ""){
				csName = MString(km_createMultipleColorSet::name());
			}

			// Create the color set
			status = meshFnEx.addColorSet(csName, true, csColor);
			MCheckStatus(status,"Error creating new color set");


			if(!inCsHandle.next()){
				break;
			}
		}


		data.setClean(plug);

	} else {
		return MStatus::kUnknownParameter;
	}

	return status;
}
