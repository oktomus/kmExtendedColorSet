#ifndef KM_PARTICLETOOL
#define KM_PARTICLETOOL

class MFnMesh;
class MStatus;
class MIntArray;
class MVectorArray;
class MDoubleArray;
class MColor;

/** Tools to manage particles
*
*/
class kmParticleTool{
public:

  /** The tool constructor
  */
  kmParticleTool(){
    colors = NULL;
    positions = NULL;
  };

  /** Create an array of the size of the mesh vertices count
  * For each vertice
  * Give the closest particle id which this one is at a distance from the vertice less than maxDistance
  * A particle can be assigned to multiple vertices
  * \param maxDistance  double  the max distance of a particle from a vertice
  * \param meshFn       MObject the mesh
  * \param resultArray  MIntArray the result array with the particles ids
  * \param usePPRadius  bool  tell if yes or no we use each particle radius
  */
  const MStatus closestParticlesInRadius(const double& maxDistance, MObject& mesh, MIntArray& resultArray, const bool usePPRadius = true, const int& maxInfluence=-1, const int& approximation=0) const;

  /** Given an id, tell if it is correct or not
  * If 0 <= id <= tool.positions.length()-1 -> Correct
  */
  const bool isValidId(const int *id) const;

  /** Check if the tool color array is valid
  */
  const bool isValidColor() const;

  /** Check if the tool position array is valid
  */
  const bool isValidPosition() const;

  /** Check if the tool radius array is valid
  */
  const bool isValidRadius() const;

  /** Create an array of color from an array of particle id
  * For each id in the base array, search the particle color from the particleTool attributes
  * And add it in the result array at the same index
  * The default color is assigned when the particle doesn't exists
  * \param particlesId    MIntArray& The base array which contains particles id
  * \param resultArray    MColorArray& The result array which contains particles color
  * \param defaultColor   MColor  the color used when no particle is found given an id
  */
  const MStatus relatedColors(MIntArray& particlesId, MColorArray& resultArray, MColor defaultColor = MColor(0,0,0,1)) const;

  /** Set the particles colors to use in the tool
  */
  const MStatus setColors(MVectorArray& colors_);

  /** Set the particles positions to use in the tool
  */
  const MStatus setPositions(MVectorArray& positions_);

  /** Set the particles radius to use in the tool
  */
  const MStatus setRadius(MDoubleArray& radius_);

private:

  /** The particles datas
  */
  MVectorArray  *colors,
                *positions;
  MDoubleArray  *radius;
};


#endif // KM_PARTICLETOOL
